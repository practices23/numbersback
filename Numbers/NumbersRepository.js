/**
 * En este apartado se hacen llamados a API externas o guardado en base de datos
 */


class NumbersRepository {

    
    async getInformation( payload ) {
        console.log(payload)
        const { initialNumber, finalNumber } = payload;
        var primos = [];
        var suma = 0;
        var count = 0;


        for (var i = initialNumber+1 ; i < finalNumber ; i++){
            count ++;
            suma= suma + i; 
        }


        const isPrime = num => {
            for(let i = 2, s = Math.sqrt(num); i <= s; i++) {
                if(num % i === 0) return false;
            }
            return num > 1;
        }

        for (var j = initialNumber ; j <= finalNumber ; j++){

            const numero = isPrime(j);
                
            if(numero){
                primos.push(j);
            }
        }
        

        return {code:200, message:'Respuestas obtenidas', data: [{primos: primos},{cantidadDeNumeros: count},{suma: suma}]}   ;
        
    
    }



}

module.exports = NumbersRepository;
