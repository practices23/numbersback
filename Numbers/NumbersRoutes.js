/**
 * 
 * Apartado donde se establecen las rutas de los servicios
 * 
 */
const { Router } = require("express");
const { validarNumber } = require("../Scripts/validateNumbers");

const NumbersController = require('./NumbersController');
const NumbersRepository = require('./NumbersRepository');
const NumbersService    = require('./NumbersService');

global.numbersRepository = new NumbersRepository();
global.numbersService    = new NumbersService(global.numbersRepository);

const router = Router();

/* Llamado a controladores */
const numbersController  = new NumbersController;

//Generar publicacion
router.post('/', validarNumber, numbersController.getInformation);



module.exports = router;
