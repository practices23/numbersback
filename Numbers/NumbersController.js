/**
 * 
 * Encargado de llamar los métodos correspondientes para las APIs
 * 
 */

const { request } = require("express");
const { response } = require("express");

class NumbersController {
    constructor(numbersRepository){        
        this.numbersRepository=numbersRepository
    }


    /**
     * Crear nueva publicacion
     * 
     * @param {*} req 
     * @param {*} res 
     * 
     */
    async getInformation(req = request, res = response ) {
        try {
            const { code, message, data } = await global.numbersService.handleGetInformation(req.body);
            return res.status(code).json({ code, message, data });
        } 
        catch (error) {
            console.log(error);
            return res.status(501).json({ code: 501, message: "Error al crear publicacion"});
        }
    }


}

module.exports = NumbersController;

