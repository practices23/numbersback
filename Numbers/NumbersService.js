/**
 * En este apartado se harán todas las funciones de lógica del negocio
 * 
 */

class NumbersService {

    constructor(numbersRepository) {
        this.numbersRepository = numbersRepository;
    }

    /**
     * Encargado de guardar información del publicacion
     * 
     * @param {Json} payload Información del publicacion
     * 
     * @returns Json
     */
    async handleGetInformation( payload ) {

        const { code, message,data } = await this.numbersRepository.getInformation(payload);
        return { code, message, data };

    }



}

module.exports = NumbersService;
