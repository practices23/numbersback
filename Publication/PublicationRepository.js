/**
 * En este apartado se hacen llamados a API externas o guardado en base de datos
 */

const Publication = require('../Schema/Publication');

class PublicationRepository {

    /**
     * Guarda en base de datos las publicaciones
     * 
     * @param { Json } payload Información de la publicacion
     *  
     * @returns { Json }
     * 
     */
    async savePublication( payload ) {

        try {
            
            const savePublication = new Publication( payload );

            const response = await savePublication.save();
            
            return { code: 201, data: response, message: 'Publicacion creada correctamente' }

        } catch (error) {
            console.log(error); 
            throw new Error(error);
        }

    } 

    /**
     * Función para obtener las publicaciones
     * 
     * 
     * @returns 
     */
    async getPublication() {

        try {

            const data = await Publication.find().sort({'createdAt':'desc'});

            if (data.length > 0 ) return { code: 200, message: 'Registros encontrado', data: data }

            return { code: 404, message: 'Registro no encontrados', data: [] };

        } catch (error) {
            console.log(error);
            throw new Error(error);
        }

    }


    /**
     * Función para obtener las publicaciones
     * 
     * 
     * @returns 
     */
    async getPublicationById(query) {

        try {

            const data = await Publication.find(query);

            if (data.length > 0 ) return { code: 200, message: 'Registro encontrado', data: data[0] }

            return { code: 404, message: 'Registro no encontrados', data: [] };

        } catch (error) {
            console.log(error);
            throw new Error(error);
        }

    }

}

module.exports = PublicationRepository;
