
/**
 * aqui se van a realizar las validaciones de los numeros
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next
 * 
 * @return {Json} 
 */


const { request, response } = require("express");


const validarNumber = async(req = request, res = response, next) =>{


    const { initialNumber, finalNumber } = req.body;
    console.log(initialNumber)
    console.log(finalNumber)

    if( !initialNumber  && !finalNumber  ){
        const code=401, message='No hay numeros en la peticion, initialNumber, finalNumber ', data=[];
        return res.status(code).json({code, message, data});
    }else if(!initialNumber){
        const code=401, message='Falta initialNumber ', data=[];
        return res.status(code).json({code, message, data});
    }else if(!finalNumber){
        const code=401, message='Falta finalNumber ', data=[];
        return res.status(code).json({code, message, data});
    }

    if( initialNumber<0  || finalNumber<0  ){
        const code=401, message='Los numeros no pueden ser negativos ', data=[];
        return res.status(code).json({code, message, data});
    }

    //Validación
    try{
        //si JWT no es valido lanza un error
        //verificar si el uid tiene estado true
        if(initialNumber > finalNumber ){
            const code=401, message='Numeros no validos, el numero inicial no puede ser mayor al numero final', data=[];
            return res.status(code).json({code, message, data});

        }

        next();

    }catch(error){
        
        console.log(error);
        const code=401, message='Numeros no valido', data=[];

        res.status(code).json({ 
            code, message, data
        })
    }
}




module.exports={
    validarNumber,
}